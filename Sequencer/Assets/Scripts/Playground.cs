﻿using JetBrains.Annotations;
using System.Collections.Generic;
using UnityEngine;

public class Playground : MonoBehaviour
{
    public int ColumnsCount = 5;
    public int RowsCount = 7;

    public int CountOfItemsPerSpawn = 3;
    public float SpawnInterval = 5f;

    public int MinimumSequentialItemsCount = 3;

    public Material[] ItemsMaterials;
    public GameObject ItemPrefab;

    private float _elapsedTimeAfterLastSpawn = 0f;
    private Item[,] _cellsCR;
    private ItemInteractor _itemInteractor;
    private int _itemTypesCount;
    private Transform _playgroundTransform;
    private float _stepY;

    [UsedImplicitly]
    public void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        _playgroundTransform = transform;
        _cellsCR = new Item[ColumnsCount, RowsCount];
        _itemTypesCount = ItemsMaterials.Length;
        CalculateStepY();
        FillBottomRow();
        _elapsedTimeAfterLastSpawn = SpawnInterval + 1f;
        _itemInteractor = new ItemInteractor();
    }

    private void CalculateStepY()
    {
        var totalDistance = RowsCount - 1;
        // _stepY * SpawnInterval = totalDistance
        _stepY = totalDistance / SpawnInterval;
    }

    private void FillBottomRow()
    {
        const int row = 0;
        var gen = new InconsequentialGenerator(0, _itemTypesCount);
        for (var col = 0; col < ColumnsCount; col++)
        {
            var itemTypeId = gen.Generate();
            _cellsCR[col, row] = SpawnItem(itemTypeId, col, row, true);
        }
    }

    private Item SpawnItem(int itemTypeId, int column, int row, bool stationary, bool interactable = false)
    {
        var itemGo = Instantiate(ItemPrefab);
        var item = itemGo.GetComponent<Item>();
        item.Initialize(itemTypeId, ItemsMaterials[itemTypeId], stationary, interactable);
        var itemTr = itemGo.transform;
        itemTr.SetParent(_playgroundTransform, false);
        var x = ColumnNum2X(column);
        var y = RowNum2X(row);
        itemTr.localPosition = new Vector3(x, y, 0);
        return item;
    }

    void FixedUpdate()
    {
        Spawn();
        MoveAllItems();
        UpdateItemsCellsPositions();
        FindSequences();
        var state = _itemInteractor.ProcessInteraction();
        if (state == ItemInteractionState.Continuing)
        {
            AttemptToFindColumnForItem(_itemInteractor.ActiveItem, _itemInteractor.X);
        }
        else if (state == ItemInteractionState.Finished)
        {
            FinalizeItemMovement(_itemInteractor.ActiveItem);
        }
    }

    private void Spawn()
    {
        _elapsedTimeAfterLastSpawn += Time.fixedDeltaTime;

        if (_elapsedTimeAfterLastSpawn < SpawnInterval) return;

        _elapsedTimeAfterLastSpawn = 0f;
        SpawnItemsAtTop();
    }

    private void SpawnItemsAtTop()
    {
        var cellsAvailabilities = new bool[ColumnsCount];
        var row = RowsCount - 1;

        for (var col = 0; col < ColumnsCount; col++)
        {
            cellsAvailabilities[col] = _cellsCR[col, row] == null;
        }

        var cellsIndexesForSpawn = Spawner.GetRandomFreeCellsIndexesForGivenItemsCountOnLine(CountOfItemsPerSpawn, cellsAvailabilities);

        var interactableItemIndex = Random.Range(0, cellsIndexesForSpawn.Length);

        for (var i = 0; i < cellsIndexesForSpawn.Length; i++)
        {
            var col = cellsIndexesForSpawn[i];
            var itemTypeId = Random.Range(0, _itemTypesCount);
            _cellsCR[col, row] = SpawnItem(itemTypeId, col, row, false, i == interactableItemIndex);
        }
    }

    private void MoveAllItems()
    {
        var dy = Vector3.down * Time.fixedDeltaTime * _stepY;
        for (var col = 0; col < ColumnsCount; col++)
        {
            //row == 0 is static. Ignoring it.
            for (var row = 1; row < RowsCount; row++)
            {
                var item = _cellsCR[col, row];
                if (item == null) continue;
                var underneathItem = _cellsCR[col, row - 1];
                if (underneathItem != null && underneathItem.Stationary)
                {
                    item.Stationary = true;
                    continue; 
                }
                item.Stationary = false;
                item.Transform.localPosition += dy;
            }
        }
    }

    private void UpdateItemsCellsPositions()
    {
         var newCells = new Item[ColumnsCount, RowsCount];

        for (var col = 0; col < ColumnsCount; col++)
        {
            for (var row = 0; row < RowsCount; row++)
            {
                var item = _cellsCR[col, row];
                if (item == null) continue;
                var pos = item.Transform.localPosition;
                // y = row + 0.5f - RowsCount * 0.5f;
                var newRow = Mathf.CeilToInt(pos.y + RowsCount * 0.5f - 0.5f);
                /*var newCol = Mathf.Clamp(
                    Mathf.RoundToInt(pos.x + ColumnsCount * 0.5f - 0.5f),
                    0, ColumnsCount - 1
                );
                if (newCells[newCol, newRow] != null)
                {
                    Debug.LogError("There are two items in one cell!");
                }*/
                newCells[col, newRow] = item;
            }
        }

        _cellsCR = newCells;
    }

    private void FindSequences()
    {
        for (var row = 0; row < RowsCount; row++)
        {
            var prevItemTypeId = -1;
            var currentSequenceLength = 0;
            for (var col = 0; col < ColumnsCount; col++)
            {
                var item = _cellsCR[col, row];

                if (
                    item != null
                    &&
                    (row != 0 && _cellsCR[col, row - 1] != null)
                    &&
                    item.TypeId == prevItemTypeId
                    &&
                    item.Stationary
                    &&
                    !item.CapturedByUser
                   )
                {
                    currentSequenceLength++;
                }
                else
                {
                    if (currentSequenceLength >= MinimumSequentialItemsCount)
                        CommitSequence(row, col - currentSequenceLength, row, col - 1);
                    currentSequenceLength = 1;
                    prevItemTypeId = item == null ? -1 : item.TypeId;
                }
            }
            if (currentSequenceLength >= MinimumSequentialItemsCount)
                CommitSequence(row, ColumnsCount - currentSequenceLength, row, ColumnsCount - 1);
        }

        for (var col = 0; col < ColumnsCount; col++)
        {
            var prevItemTypeId = -1;
            var currentSequenceLength = 0;
            for (var row = 0; row < RowsCount; row++)
            {
                var item = _cellsCR[col, row];

                if (
                    item != null
                    &&
                    (row != 0 && _cellsCR[col, row - 1] != null)
                    &&
                    (item.TypeId == prevItemTypeId)
                    &&
                    item.Stationary
                    &&
                    !item.CapturedByUser
                   )
                {
                    currentSequenceLength++;
                }
                else
                {
                    if (currentSequenceLength >= MinimumSequentialItemsCount)
                        CommitSequence(row - currentSequenceLength, col, row - 1, col);
                    currentSequenceLength = 1;
                    prevItemTypeId = item == null ? -1 : item.TypeId;
                }
            }
            if (currentSequenceLength >= MinimumSequentialItemsCount)
                CommitSequence(RowsCount - currentSequenceLength, col, RowsCount - 1, col);
        }
    }

    private void CommitSequence(int rowStart, int columnStart, int rowEnd, int columnEnd)
    {
        for (var row = rowStart; row <= rowEnd; row++)
        {
            for (var col = columnStart; col <= columnEnd; col++)
            {
                Destroy(_cellsCR[col, row].gameObject);
            }
        }
    }

    private void AttemptToFindColumnForItem(Item item, float x)
    {
        item.Stationary = false;
        var pos = item.Transform.position;

        x = Mathf.Clamp(x, 0.5f * (1 - ColumnsCount), 0.5f * (ColumnsCount - 1));
        var col = Mathf.RoundToInt(x + ColumnsCount * 0.5f - 0.5f);
        var dx = x - (col + 0.5f - ColumnsCount * 0.5f);
        var row = Mathf.CeilToInt(pos.y + RowsCount * 0.5f - 0.5f);

        var z = 0f;
        if (_cellsCR[col, row] != null && _cellsCR[col, row].Transform != item.Transform)
        {
            z = dx * dx * 4f - 1f;
        }
        else
        {
            var cr = FindItemOnPlaygroundCr(item);
            _cellsCR[cr[0], cr[1]] = null;
            _cellsCR[col, row] = item;
        }

        var newPos = new Vector3(x, pos.y, z);

        item.Transform.position = newPos;
    }

    private Vector2Int FindItemOnPlaygroundCr(Item item)
    {
        for (var row = 0; row < RowsCount; row++)
            for (var col = 0; col < ColumnsCount; col++)
                if (_cellsCR[col, row] != null && _cellsCR[col, row].Equals(item))
                    return new Vector2Int(col, row);
        return new Vector2Int(-1, -1);
    }

    private void FinalizeItemMovement(Item item)
    {
        StartCoroutine(FinalizeItemMovementAsync(item));
    }

    IEnumerator<Item> FinalizeItemMovementAsync(Item item)
    {
        var targetCr = FindItemOnPlaygroundCr(item);
        var targetCol = targetCr[0];
        var targetX = ColumnNum2X(targetCol);
        var delta = targetX - item.Transform.localPosition.x;
        while (Mathf.Abs(delta) > 0.01f)
        {
            var pos = item.Transform.localPosition;

            var newX = pos.x + delta * 0.5f;

            var coveredCol = Mathf.RoundToInt(newX + ColumnsCount * 0.5f - 0.5f);

            var coveredItem = _cellsCR[coveredCol, targetCr[1]];

            var newZ = 0f;

            if (coveredItem != null && coveredItem.Transform != item.Transform)
            {
                newZ = 1f;
            }

            item.Transform.localPosition = new Vector3(newX, pos.y, newZ);

            delta = targetX - item.Transform.localPosition.x;
            yield return null;
        }
        item.Transform.localPosition = new Vector3(
            targetX,
            item.Transform.localPosition.y,
            item.Transform.localPosition.z
        );
        item.Stationary = true;
        item.CapturedByUser = false;
        yield return null;
    }

    private float ColumnNum2X(int columnNum)
    {
        return columnNum + 0.5f - ColumnsCount * 0.5f;
    }

    private float RowNum2X(int rowNum)
    {
        return rowNum + 0.5f - RowsCount * 0.5f;
    }
}
