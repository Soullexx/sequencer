﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemInteractor
{
    public Item ActiveItem;
    public float X;

    private delegate ItemInteractionState ItemInteraction();
    private ItemInteraction _do;

    public ItemInteractor()
    {
        _do = Idle;
    }

    public ItemInteractionState ProcessInteraction()
    {
        return _do();
    }

    private ItemInteractionState Idle()
    {
        if (!Input.GetMouseButtonDown(0)) return ItemInteractionState.None;

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        var layerMask = 1 << 8;
        if (!Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask)) return ItemInteractionState.None;

        ActiveItem = hit.transform.parent.GetComponent<Item>();
        ActiveItem.CapturedByUser = true;

        _do = PerformDrag;
        return _do();
    }

    private ItemInteractionState PerformDrag()
    {
        if (Input.GetMouseButtonUp(0))
        {
            _do = FinalizeDrag;
            return ItemInteractionState.Finished;
        }

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        var layerMask = 1 << 9;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            X = hit.point.x;
        }
               
        return ItemInteractionState.Continuing;
    }

    private ItemInteractionState FinalizeDrag()
    {
        ActiveItem = null;
        _do = Idle;
        return ItemInteractionState.None;
    }
}
