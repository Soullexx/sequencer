﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner
{
    public static int[] GetRandomFreeCellsIndexesForGivenItemsCountOnLine(int itemsCount, bool[] cellsAvailabilities)
    {
        var freeCellsIndexes = GetFreeCellsIndexes(cellsAvailabilities);

        if (itemsCount > freeCellsIndexes.Count)
        {
            Debug.LogWarning("Application has requested to create more elements than the number of free cells. A number of elements were created equal to the number of free cells.");
            itemsCount = freeCellsIndexes.Count;
        }

        var res = new int[itemsCount];

        for (var i = 0; i < itemsCount; i++)
        {
            var n = Random.Range(0, freeCellsIndexes.Count);
            res[i] = freeCellsIndexes[n];
            freeCellsIndexes.RemoveAt(n);
        }

        return res;
    }

    private static List<int> GetFreeCellsIndexes(bool[] cellsAvailabilities)
    {
        var res = new List<int>();

        for (var i = 0; i < cellsAvailabilities.Length; i++)
        {
            if (cellsAvailabilities[i])
                res.Add(i);
        }

        return res;
    }
}
