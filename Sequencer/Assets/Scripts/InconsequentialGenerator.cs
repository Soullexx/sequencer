﻿using UnityEngine;

public class InconsequentialGenerator
{
    private readonly int _max;
    private readonly int _min;
    private int _prevPrev = -1;
    private int _prev = -1;

    public InconsequentialGenerator(int min, int max)
    {
        _max = max;
        _min = min;
    }

    public int Generate()
    {
        while (true)
        {
            var n = Random.Range(_min, _max);
            if (_prevPrev == n && _prev == n) continue;
            _prevPrev = _prev;
            _prev = n;
            return n;
        }
    }
}
