﻿using UnityEngine;

public class Item : MonoBehaviour
{
    public int TypeId { get; private set; }
    public Transform Transform { get; private set; }
    public bool Stationary;
    public bool CapturedByUser;

    public void Initialize(int typeId, Material material, bool stationary, bool interactable = false)
    {
        TypeId = typeId;
        var mr = GetComponent<MeshRenderer>();
        if (mr == null)
        {
            Debug.LogError("There is no MeshRenderer on the item. Initialization of the item failed.");
            return;
        }

        mr.material = material;

        Stationary = stationary;

        Transform = transform;

        if (!interactable)
        {
            Destroy(Transform.GetChild(0).gameObject);
        }
    }
}
